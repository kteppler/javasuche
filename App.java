
import java.io.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class App {

	public static void main(String[] args) {
		JavaSuchergebnis suche;
		File f = new File("e:\\Java");
		String dateiName = "e:\\Java\\Suche.ser";
		Scanner scan =new Scanner(System.in);
		System.out.println("Suchbegriff:");
		String suchBegriff = scan.next();
		System.out.println("Dateiendung:");
		String dateiEndung = scan.next(); //".java";
		suche = new JavaSuchergebnisBuilder()
				.setFileList(SearchFiles.search(
						f, 
						new ArrayList<>(), 
						s->s.endsWith(dateiEndung)&&s.contains(suchBegriff))
						)
				.setSuchBegriff(suchBegriff)
				.build();
		exportCSV(dateiName, suche);
		scan.close();
		suche = null;
		//ausgabe(laden(dateiName));
		
	}
	
	static void speichern(String dateiName, JavaSuchergebnis suche){
		try {
			FileOutputStream fos = new FileOutputStream(dateiName); 
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(suche);
			os.close();
			fos.close();
		}catch (Exception e) {
		    e.printStackTrace();
		}
	}
	
	static void exportCSV(String dateiName, JavaSuchergebnis suche){
		try {
			PrintWriter writer = new PrintWriter("e:\\Java\\suche.csv");
			writer.println(
					"Laufwerk: "
					+";"
					+"Pfad: "
					+";"
					+"Dateiname: "
					);	
			for (File element : suche.getFileList()) {
				writer.println(
						element.getAbsolutePath().substring(0,2)
						+";"
						+element.getAbsolutePath().replace(element.getName(),"").substring(3)
						+";"
						+element.getName()				
						);	
			}
			writer.close();
		}catch (Exception e) {
		    e.printStackTrace();
		}
	}
	static JavaSuchergebnis laden(String dateiName) {
		try {
			FileInputStream fis = new FileInputStream(dateiName);
			ObjectInputStream is = new ObjectInputStream(fis);
			JavaSuchergebnis suche =  (JavaSuchergebnis) is.readObject();
			is.close();
			fis.close();
			return suche;
		}catch (Exception e) {
	        e.printStackTrace();
		}
		return null;
	}
	
	static void ausgabe(JavaSuchergebnis suche) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");  
		System.out.println("Suche vom: "+dtf.format(suche.getSucheVom()));
		System.out.println("Suchbegriff: "+ suche.getSuchBegriff());
		for (File file : suche.getFileList()) {
			System.out.print(
					"\n <Directory> "
					+file.getAbsolutePath().replace(file.getName(),"")
					+"\n"
					+"\t <file> "+file.getName()
					+"\t <size> "+file.length()
					+" kb"
					);
		}
		
	}

}
