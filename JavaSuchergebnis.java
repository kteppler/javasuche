
import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class JavaSuchergebnis implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4227577256900391081L;
	private String suchBegriff;
	private LocalDateTime sucheVom;
	private ArrayList<File> fileList = new ArrayList<>();
	
	public JavaSuchergebnis() {}
	public JavaSuchergebnis(String suchBegriff, ArrayList<File> fileList) {
		this.suchBegriff = suchBegriff;
		this.fileList = fileList;
		this.sucheVom = LocalDateTime.now();
	}
	public String getSuchBegriff() {
		return suchBegriff;
	}
	public void setSuchBegriff(String suchBegriff) {
		this.suchBegriff = suchBegriff;
	}
	public LocalDateTime getSucheVom() {
		return sucheVom;
	}
	public ArrayList<File> getFileList() {
		return fileList;
	}
	public void setFileList(ArrayList<File> fileList) {
		this.fileList = fileList;
	}
}
