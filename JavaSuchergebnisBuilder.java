
import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class JavaSuchergebnisBuilder {
	String suchBegriff;
	LocalDateTime sucheVom;
	ArrayList<File> fileList = new ArrayList<>();
	
	JavaSuchergebnisBuilder(){};
	
	public JavaSuchergebnisBuilder setSuchBegriff(String suchBegriff) {
		this.suchBegriff = suchBegriff;
		return this;
	}
	
	public JavaSuchergebnisBuilder setFileList(ArrayList<File> fileList) {
		this.fileList = fileList;
		return this;
	}
	
	public JavaSuchergebnis build() {
		return new JavaSuchergebnis(suchBegriff, fileList);
	}
}
