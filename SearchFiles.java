
import java.io.File;
import java.util.ArrayList;
import java.util.function.Predicate;

public class SearchFiles {
	static ArrayList<File> search(File dir, ArrayList<File> list, Predicate<String> suchBedingung) {
		File[] fileArray = dir.listFiles();
		if (fileArray != null) {
			for (File element : fileArray) {
				if (element.isDirectory()) {
					search(element, list, suchBedingung);
				} else {
					
					if(suchBedingung.test(element.getName())) {
						list.add(element);
					}
				}
			}
		}
		return list;
	}
}
